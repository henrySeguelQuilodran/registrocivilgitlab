package page;



import java.util.Set;
import java.util.Iterator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import logic.UtilityClass;



public class OutlookReunionPage extends UtilityClass {
	
	By campoUserNameSelector = By.id("username");
	By campoPasswordSelector = By.id("password");
	By btnInicioSesionSelector = By.xpath("//input[@type='submit']");
    By paginaLoginSelector = By.xpath("//img[@src='/owa/14.3.513.0/themes/resources/lgntopl.gif']");
   
    By btnMensajeReunionSelector = By.xpath("//img[@id='imgToolbarButtonDropdownIcon']");
    By btnNuevaReunionSelector = By.xpath("//a[@id='newmtng']");

    By campoDestinatarioSelector = By.xpath("//div[@id='divTo']");
    By campoOpcionalSelector = By.xpath("//div[@id='divFieldCc']");
    By campoRecursosSelector = By.xpath("//div[@id='divWellBcc']");
    By campoAsuntoSelector = By.xpath("//input[@id='txtSubj']");
    By campoUbicacionSelector = By.xpath("//input[@id='txtLoc']");
    
    By btnCalendarioInicioSelector = By.xpath("//input[@id='txtTime']");
    
    By campofechaInicioReunionSelector = By.xpath("//span[@id='spanCmbSel']");
    //By campofechaInicioReunionSelector = By.xpath("//div[@id='divSDate']");
    By campoHoraInicioReunioSelector = By.xpath("//div[@id='divStartTime']");
    
    By campofechaTerminoReunionSelector = By.xpath("//div[@id='divEDate']");
    By campoHoraTerminoReunioSelector = By.xpath("//div[@id='divEndTime']");
    
    By campoAvisoReunionSelector = By.xpath("//div[@id='divRmdTime']");
    By campoEstadoParticipanteSelector = By.xpath("//div[@id='divBsyType']");
    
    By cuerpoMensajeSelector = By.xpath("//*[@id=\'ifBdy\']");
    //By cuerpoMensajeSelector = By.xpath("//div[@id="divBdy"]");  
    //By cuerpoMensajeSelector = By.xpath("//textarea[@id='txtBdy']");
    By btnEnviarMensajeSelector = By.xpath("//a[@id='send']");
  
    
	public OutlookReunionPage(WebDriver driver) {
		super(driver);
		
	}

	
	public void envioEMailAutomatizado () throws InterruptedException {
	
		
		if(isDisplayed(paginaLoginSelector)) {
			type("henry.quilodran", campoUserNameSelector);
			type("ban9PtHg", campoPasswordSelector);
			click(btnInicioSesionSelector);
			Thread.sleep(2000);
			click(btnMensajeReunionSelector);
			//click(btnNewMessageSelector);
			Thread.sleep(2000);
			click(btnNuevaReunionSelector);
			Thread.sleep(2000);
			
			
			
			//manejo de la ventana emergente
			String parentWindowHandler = driver.getWindowHandle(); // Almacena tu ventana actual
			String subWindowHandler = null;
			Set<String> handles = driver.getWindowHandles(); // Obten todas las ventana abiertas
			Iterator<String> iterator = handles.iterator();
			while (iterator.hasNext()){
			    subWindowHandler = iterator.next();
			}
			
			driver.switchTo().window(subWindowHandler); // C�mbiate a la ultima ventana (tu pop-up)
		
			//  Informaci�n  pop-up					
			type("emilio.silva@colmena.cl",campoDestinatarioSelector);
//			type("",campoOpcionalSelector);
//			type("", campoRecursosSelector);			
			type("Invitacion Automatizada  a Reuni�n",campoAsuntoSelector);
			type("Slack",campoUbicacionSelector);
				
//			click(btnCalendarioInicioSelector);
					
//			type("04022022", campofechaInicioReunionSelector);			
//			type("2200",campoHoraInicioReunioSelector);
		
//			type("mi� 02-02-2022",campofechaTerminoReunionSelector);
//			type("11:30",campoHoraTerminoReunioSelector);
			
//			type("",campoAvisoReunionSelector);
//			type("",campoEstadoParticipanteSelector);
			
			type("Hola, este es un mensaje automatizado", cuerpoMensajeSelector);
	
			click(btnEnviarMensajeSelector);
			Thread.sleep(2000);
		
			//driver.switchTo().window(parentWindowHandler);  // Vuelve a tu ventana principal (si lo necesitas)
			
	
		}	
	}
}