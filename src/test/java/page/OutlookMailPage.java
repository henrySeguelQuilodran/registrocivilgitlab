package page;

import java.util.Set;
import java.util.Iterator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



import logic.UtilityClass;

public class OutlookMailPage extends UtilityClass {
	
	By campoUserNameSelector = By.id("username");
	By campoPasswordSelector = By.id("password");
	By btnInicioSesionSelector = By.xpath("//input[@type='submit']");
    By paginaLoginSelector = By.xpath("//img[@src='/owa/14.3.513.0/themes/resources/lgntopl.gif']");
   
    
    By btnNewMessageSelector = By.xpath("//a[@id='newmsgc']");
    
    By formNewMailSelector = By.xpath("//a[@id='messageoptions']");
   
    By campoDestinatarioSelector = By.xpath("//div[@id='divTo']");
    By campoConCopiaSelector = By.xpath("//div[@id='divCc']");
    By campoAsuntoSelector = By.xpath("//input[@id='txtSubj']");
    By cuerpoMensajeSelector = By.xpath("//*[@id=\'ifBdy\']");
    
    By btnEnviarMensajeSelector = By.xpath("//a[@id='send']");
    
    
	public OutlookMailPage(WebDriver driver) {
		super(driver);
		
	}

	public void envioEMailAutomatizado () throws InterruptedException {
	
		
		if(isDisplayed(paginaLoginSelector)) {
			type("henry.quilodran", campoUserNameSelector);
			type("ban9PtHg", campoPasswordSelector);
			click(btnInicioSesionSelector);
			Thread.sleep(2000);
			click(btnNewMessageSelector);
			Thread.sleep(2000);
			
			//manejo de la ventana emergente
			
			String parentWindowHandler = driver.getWindowHandle(); // Almacena tu ventana actual
			String subWindowHandler = null;
			Set<String> handles = driver.getWindowHandles(); // Obten todas las ventana abiertas
			Iterator<String> iterator = handles.iterator();
			while (iterator.hasNext()){
			    subWindowHandler = iterator.next();
			}
			driver.switchTo().window(subWindowHandler); // C�mbiate a la ultima ventana (tu pop-up)
			
		
			// Aqu� rellena la informaci�n referente a tu pop-up					
			type("hseguelq@gmail.com",campoDestinatarioSelector);
			type("hseguelq@gmail.com",campoConCopiaSelector);
			type("E-Mail Automatizado", campoAsuntoSelector);
			type("Hola, este es un mensaje automatizado", cuerpoMensajeSelector);
			
			click(btnEnviarMensajeSelector);
			Thread.sleep(2000);
			
			driver.switchTo().window(parentWindowHandler);  // Vuelve a tu ventana principal (si lo necesitas)
			
	
		}	
	}
}