package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import page.OutlookMailPage;


public class OutlookMailTest {
	
	private WebDriver driver;
	OutlookMailPage outlookMailPage;

	@Before
	public void setUp() throws Exception {
		outlookMailPage = new OutlookMailPage (driver);
		driver = outlookMailPage.chromeDriverConexion();
		outlookMailPage.visit("https://webmail.colmena.cl/owa/auth/logon.aspx?replaceCurrent=1&url=https%3a%2f%2fwebmail.colmena.cl%2fowa%2f");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws InterruptedException {
		outlookMailPage.envioEMailAutomatizado();
	}

}
