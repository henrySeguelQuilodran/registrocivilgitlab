package test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.testng.annotations.Test;

public class Clase {

	@Test
    public void dateTimePicker() throws InterruptedException{
        System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        Thread.sleep(3000);
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://demo.guru99.com/test/");
        //Buscar el control datepicker 
        WebElement dateBox = driver.findElement(By.xpath("//form//input[@name='bdaytime']"));
        //llenar la fecha (dd/mm/aaaa) sin delimiter (/) 
        dateBox.sendKeys("02022022");
        //Presione la tecla Tab para desplazar el foco al campo time
        dateBox.sendKeys(Keys.TAB);
        //llenar el tiempo con 02:45 PM
        dateBox.sendKeys("0645PM");
    }
    }
