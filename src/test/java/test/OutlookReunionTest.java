package test;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import page.OutlookReunionPage;


public class OutlookReunionTest {
	
	private WebDriver driver;
	OutlookReunionPage outlookReunionPage;

	@Before
	public void setUp() throws Exception {
		outlookReunionPage = new OutlookReunionPage (driver);
		driver = outlookReunionPage.chromeDriverConexion();
		outlookReunionPage.visit("https://webmail.colmena.cl/owa/auth/logon.aspx?replaceCurrent=1&url=https%3a%2f%2fwebmail.colmena.cl%2fowa%2f");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws InterruptedException {
		outlookReunionPage.envioEMailAutomatizado();
	}

}
